var ws = ig.game.O868.ws

var keyMsgs = []
var useKeyboard = true // turns out guitar only has 33 unique notes while keyboard has 38
if (typeof useKeyboard != "undefined") {
	var octaves = "=;>".split('')
	var notes = "c cs d ds e f fs g gs a as b".split(' ')
	for (let o of octaves)
		for (let n of notes) 
			keyMsgs.push(`"m":"xf"U"i":"classic_piano*"o":${o},"n":"${n}?`)
} else /*use guitar*/ {
	var guitarKs = ">6 >7 >8 >9 )( )= ); )< )> >= >; >< >> >) <; << <> <) <6 ;< ;> ;) ;6 ;7 =) =6 =7 =8 ) 6 7 8 9".split(' ')
	guitarKs.forEach(k => keyMsgs.push(`"m":"xf"U"i":"guitar*"k":${k}%`))
}


var OFFSET = 15; // use this to adjust the translation of the narrow keyboard on the full 88 key range. I think 25 puts it in the middle ( 88/2 - (38/2) = 25 ). 15 makes notes match.

navigator.requestMIDIAccess().then(midi => {
	window.midi = midi;
	window.midiinput = midi.inputs.values().next().value;
	midiinput.onmidimessage = function midimessagehandler(evt) {
		var channel = evt.data[0] & 0xf;
		var note_number = evt.data[1];
		var cmd = evt.data[0] >> 4;
		var vel = evt.data[2];
		if(cmd == 8 || (cmd == 9 && vel == 0)) {
			// NOTE_OFF
		} else if(cmd == 9) {
			// NOTE_ON
			//press(MIDI_KEY_NAMES[note_number - 9 + MIDI_TRANSPOSE], vel / 100);
			let keyMsg = keyMsgs[note_number - 21 - OFFSET];
			if (keyMsg) ws.send(keyMsg);
		} else if(cmd == 11) {
			// CONTROL_CHANGE
		}
	}
})